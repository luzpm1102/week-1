//ELEMENTS DECLARATION
const form = document.getElementById('form');
const username = document.getElementById('username');
const age = document.getElementById('age');
const birthday = document.getElementById('birthday');
const radio = document.getElementsByName('gender');
const email = document.getElementById('email');
const password = document.getElementById('password');
const passwordCheck = document.getElementById('passwordCheck');
const phone = document.getElementById('phone');
const website = document.getElementById('website');
const react = document.getElementById('react');
const vue = document.getElementById('vue');
const angular = document.getElementById('angular');
const another = document.getElementById('another');
const frameworkList = document.getElementsByClassName('framework-checkbox');
const checkboxes = document.getElementById('container-section-2');
//STEP 1
let errors = [];

let validInfo = [];
form.addEventListener('submit', (e) => {
  e.preventDefault();
  checkInputs();

  console.log(errors);

  if (errors.length === 0) {
    hideAndClearRanges();
    console.clear();
    console.log('INSERTED DATA', validInfo);
    alert('Data inserted! You can checked it in the console');
    form.reset();
  } else alert('INCOMPLETE OR INVALID DATA');

  errors = [];
  validInfo = [];
});

const checkInputs = () => {
  const userNameValue = username.value.trim();
  const ageValue = age.value.trim();
  const birthdayValue = birthday.value.trim();
  let radioValue = radio;
  const emailValue = email.value.trim();
  const passwordValue = password.value.trim();
  const passwordCheckValue = passwordCheck.value.trim();
  const phoneValue = phone.value.trim();
  const websiteValue = website.value.trim();
  const reactId = react.id;
  const vueId = vue.id;
  const angularId = angular.id;
  const anotherId = another.id;

  if (userNameValue === '') {
    setErrorFor(username, 'Name can not be empty');
  } else {
    setSuccessFor(username);
  }

  if (!ageValue) {
    setErrorFor(age, 'age can not be empty');
  } else if (!isAgeValid(ageValue)) {
    setErrorFor(age, 'enter a valid number');
  } else {
    setSuccessFor(age);
  }
  if (!birthdayValue) {
    setErrorFor(birthday, 'date can not be empty');
  } else {
    setSuccessFor(birthday);
  }
  radioValue = checkRadioValue(radioValue);
  if (emailValue === '') {
    setErrorFor(email, 'email can not be empty');
  } else if (!isEmail(emailValue)) {
    setErrorFor(email, 'email must be valid');
  } else {
    setSuccessFor(email);
  }
  if (!phoneValue) {
    setErrorFor(phone, 'enter a phone number');
  } else if (phoneValue.length > 8)
    setErrorFor(phone, 'enter a valid number 12345678');
  else {
    setSuccessFor(phone);
  }

  if (passwordCheckValue && passwordValue) {
    if (passwordCheckValue !== passwordValue) {
      setErrorFor(password, 'password must match');
      setErrorFor(passwordCheck, 'check must match');
    } else {
      setSuccessFor(password);
      setSuccessFor(passwordCheck);
    }
  } else if (!passwordCheckValue) {
    if (!passwordValue) {
      setErrorFor(password, 'password required');
      setErrorFor(passwordCheck, 'check required');
    } else {
      setSuccessFor(password);
      setErrorFor(passwordCheck, 'check required');
    }
  } else if (!passwordValue) {
    if (!passwordCheckValue) {
      setErrorFor(password, 'password required');
      setErrorFor(passwordCheck, 'check required');
    } else {
      setSuccessFor(passwordCheck);
      setErrorFor(password, 'password required');
    }
  }

  if (!websiteValue) {
    setErrorFor(website, 'url required');
  } else if (!isValidURL(websiteValue)) {
    setErrorFor(website, 'url not valid');
  } else {
    setSuccessFor(website);
  }
  if (checkboxValidation() === 0) {
    const small = checkboxes.querySelector('small');
    const message = 'at least one must be selected';

    small.innerText = message;
    checkboxes.className = 'form-body error';
    if (!errors.includes(message)) {
      errors.push(message);
    }
    return;
  } else {
    checkboxes.className = 'form-body success';
  }
  if (reactId !== '') {
    getRangeSelectedValue(reactId);
  }
  if (vueId !== '') {
    getRangeSelectedValue(vueId);
  }
  if (angularId !== '') {
    getRangeSelectedValue(angularId);
  }
  if (anotherId !== '') {
    getRangeSelectedValue(anotherId);
  }
};
//VALIDATIONS FUNCTIONS

const setErrorFor = (input, message) => {
  const formBody = input.parentElement;
  const small = formBody.querySelector('small');
  small.innerText = message;
  formBody.className = 'form-body error';
  if (!errors.includes(message)) {
    errors.push(message);
  }
};
const setSuccessFor = (input) => {
  const formBody = input.parentElement;
  formBody.className = 'form-body success';
  if (!validInfo.includes(input.id)) {
    const inputId = input.id;
    let inputs = Object.assign({}, [inputId, input.value]);
    validInfo.push(inputs);
  }
};

const isEmail = (email) => {
  return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    email
  );
};
const isAgeValid = (age) => {
  return /^(\d?[1-9]|[1-9]0)$/.test(age);
};

const checkRadioValue = (radio) => {
  let radioValue = '';
  for (i = 0; i < radio.length; i++) {
    if (radio[i].checked) {
      radioValue = radio[i].value;
      setSuccessFor(radio[i]);
      return radioValue;
    } else radioValue = radio[i];
  }
  return setErrorFor(radioValue, 'select a gender');
};

const isValidURL = (url) => {
  let response = url.match(
    /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
  );
  return response !== null;
};

const toggleRangeVisibility = (inputId) => {
  const input = document.getElementById(inputId);
  const range = document.getElementById(`range-${inputId}`);

  if (input.checked) {
    range.style.visibility = 'visible';
  } else {
    range.style.visibility = 'hidden';
  }
};

const hideAndClearRanges = () => {
  const range = document.getElementsByName('range');
  console.log(range);
  if (range) {
    for (let i = 0; i < range.length; i++) {
      range[i].style.visibility = 'hidden';
    }
  }
};

const getRangeSelectedValue = (inputId) => {
  const input = document.getElementById(inputId);
  const range = document.getElementById(`range-${inputId}`);

  if (input.checked) {
    if (input.id === 'another') {
      const inputText = document.getElementById('anothervalue');

      if (inputText.value === '') {
        setErrorFor(inputText, 'If selected write a value');
        return;
      } else {
        if (!validInfo.includes(inputText.id)) {
          let inputs = Object.assign({}, [inputText.value, range.value]);
          validInfo.push(inputs);
        }
        return;
      }
    }
    setSuccessFor(range);
  } else {
    return;
  }
};

const checkboxValidation = () => {
  let checkboxList = [...frameworkList];

  checkboxList = checkboxList.filter((checkbox, index) => {
    if (checkbox.checked) {
      return checkboxList[index];
    }
  });

  if (checkboxList.length > 0) {
    return checkboxList;
  } else {
    return checkboxList.length;
  }
};

const passwordValidation = (password) => {
  return /(?=.*?[A-Z])(?=.*?\d)(?=.*?\w).{8}$/.test(password);
};
//STEP 2
let konamiCode = ['ArrowUp', 'ArrowDown', '1', 'a', 'Shift', 'A'];
let coincidences = 0;

const keyHandler = (event) => {
  if (
    konamiCode.indexOf(event.key) < 0 ||
    event.key !== konamiCode[coincidences]
  ) {
    coincidences = 0;
    return;
  }

  coincidences++;

  if (konamiCode.length === coincidences) {
    coincidences = 0;
    document.getElementById('gif').style.visibility = 'visible';
  }
};

document.addEventListener('keydown', keyHandler, false);
